set terminal png size 1366,768
set output "error comparison.png"
set title "Porównanie błędów względem względem funkcji czasu t"
set xlabel "t"
set ylabel "Bład"
plot "Tekstowo/blad_max_dla_KMB.txt" using 1:2 with lines ls 1 title "Analityczne",\
  "Tekstowo/blad_max_dla_laasonen_LU.txt" using 1:2 with lines ls 7 title "Lassonen -> LU",\
  "Tekstowo/blad_max_dla_laasonen_thomas.txt" using 1:2 with lines ls 10 title "Lassonen -> Thomas"