set terminal png size 1366,768
set output "Bledy.png"
set title "Porównanie błędów metod"
set xlabel "log10(dt)"
set ylabel "log10(bledu)"
plot "bledy.txt" using 1:2 with lines ls 1 title "Metoda Bezpośrednia Euler",\
  "bledy.txt" using 1:3 with lines ls 7 title "Metoda Pośrednia Euler",\
  "bledy.txt" using 1:4 with lines ls 10 title "Metoda Trapezów"