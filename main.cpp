#include<iostream>
#include<math.h>
#include <fstream>

#define _USE_MATH_DEFINES

using namespace std;

double tau = double(0.1);
double D = double(1);
double t_max = double(2);
double x_max = double(6 * sqrt(D * (t_max+tau)));

//lambda dla metody pośredniej
double lambda = 1;
//lambda dla metody bezposredniej
double lambda2 = 0.4;



class Method {
public:
    double **laasonen, **bezpo, **analit, **analit2, **blad, *blad_max, *blad_max2, **blad2;

    Method();

    int ile_x;
    double h;
    double dt, dt2;
    int ile_t, ile_t2;

    void rozw_analityczne_lambda1(fstream &file6);

    void rozw_analityczne_lambda04(fstream &file7);

    void rozw_laasonen(int a, fstream &file11);

    void klas_met_bezp(fstream &file2);

    void thomas(double **A, double *b, double *wyn);

    void dekompozycja_LU(double **A, double *b, double *wyn);

    void f_blad(int a, fstream &file3);

    void f_blad2(fstream &file2);

    void zapis_do_pliku_wynik(char *nazwa, double **macierz, double dt, int ile_t);

    void zapis_do_pliku_blad(char *nazwa, double *wektor, double dt, int ile_t);


    double warunek_poczatkowy(double, double, double x);
};

void Method::dekompozycja_LU(double **A, double *b, double *wyn) {
//Dekompozycja LU macierzy- metoda eliminacji Gaussa
    double x;
    for (int k = 0; k < ile_x - 1; k++) {
        for (int i = k + 1; i < ile_x; i++) {
            x = A[i][k] / A[k][k];
            A[i][k] = x;
            for (int j = k + 1; j < ile_x; j++) {
                A[i][j] = A[i][j] - (x * A[k][j]);
            }
        }
    }
    double suma;
    double *z = new double[ile_x];
    for (int i = 0; i < ile_x; i++) {
        suma = 0;
        for (int j = 0; j <= i - 1; j++) {
            suma += A[i][j] * z[j];
        }

        z[i] = b[i] - suma;
    }

    for (int i = ile_x - 1; i >= 0; i--) {
        suma = 0;
        for (int j = i + 1; j < ile_x; j++) {
            suma += A[i][j] * wyn[j];
        }

        wyn[i] = (z[i] - suma) / A[i][i];
    }
}

double Method::warunek_poczatkowy(double D, double tau, double x) {
    return (1 / (2 * sqrt(M_PI * D * tau))) * exp((-1 * x * x) / (4 * D * tau));
}

void Method::klas_met_bezp(fstream &file2) {
    double t = 0, x = 0;
    for (int j = 0; j < ile_x; j++)  //warunek poczatkowy
    {
        bezpo[0][j] = warunek_poczatkowy(D, tau, j);
    }
    for (int i = 0; i < ile_t2; i++) //warunek brzegowy dla x0
    {
        bezpo[i][0] = 0;
    }
    for (int i = 0; i < ile_t2; i++) //warunek brzegowy dla x ostatniego
    {
        bezpo[i][ile_x - 1] = 0;
    }
    for (int i = 1; i < ile_t2; i++) {
        for (int j = 1; j < ile_x; j++) {
            bezpo[i][j] = bezpo[i - 1][j] + lambda2 * (bezpo[i - 1][j - 1] - 2 * bezpo[i - 1][j] + bezpo[i - 1][j + 1]);
            file2 <<t <<"\t" << x << "\t" << bezpo[i][j] << endl;
        }
    }
    zapis_do_pliku_wynik("Tekstowo/rozw_klasyczna_met_bezposrednia.txt", bezpo, dt2, ile_t2);
}

Method::Method() //konstruktor wylicza przedzialy i alokuje pamiec na macierze rozwiazan
{
    ile_x = 50;
    h = x_max / ile_x;
    dt = h * h * lambda / D;
    ile_t = static_cast<int> ((t_max / dt) + 1 );

    dt2 = h * h * lambda2 / D;
    ile_t2 = static_cast<int> ((t_max / dt2) + 1 );

    laasonen = new double *[ile_t];
    for (int i = 0; i < ile_t; i++)
        laasonen[i] = new double[ile_x];

    bezpo = new double *[ile_t2];
    for (int i = 0; i < ile_t2; i++)
        bezpo[i] = new double[ile_x];

    analit = new double *[ile_t];
    for (int i = 0; i < ile_t; i++)
        analit[i] = new double[ile_x];

    analit2 = new double *[ile_t2];
    for (int i = 0; i < ile_t2; i++)
        analit2[i] = new double[ile_x];

    blad = new double *[ile_t];
    for (int i = 0; i < ile_t; i++)
        blad[i] = new double[ile_x];

    blad2 = new double *[ile_t2];
    for (int i = 0; i < ile_t2; i++)
        blad2[i] = new double[ile_x];

    blad_max = new double[ile_t];
    blad_max2 = new double[ile_t2];
}

double analitic(double D, double t, double x, double tau) {
    return (1 / (
            2 * sqrt(
                    M_PI * D * (t + tau)
            )
    ))
           * exp(
            (-1 * x * x) / (
                    4 * D * (tau + t)
            )
    );
}

void Method::rozw_analityczne_lambda1(fstream &file7) {
    double t = 0, x = 0;
    for (int j = 0; j < ile_t; j++) {
        for (int i = 0; i < ile_x; i++) {
            analit[j][i] = analitic(D, t, x, tau); // rozwiazanie analityczne
            x = x + h;
            file7 <<t <<"\t" << x << "\t" << analit[j][i] << endl;
        }
        file7 << "\n";
        x = 0;
        t = t + dt;
    }
    zapis_do_pliku_wynik("Tekstowo/roz_analityczne_lambda_1.txt", analit, dt, ile_t);
}

void Method::rozw_analityczne_lambda04(fstream &file6) {
    double t = 0, x = 0;
    for (int j = 0; j < ile_t2; j++) {
        for (int i = 0; i < ile_x; i++) {
            analit2[j][i] = analitic(D, t, x, tau); // rozwiazanie analityczne dla lambda 0.4
            x = x + h;
            file6 <<t <<"\t" << x << "\t" << analit2[j][i] << endl;
        }
        x = 0;
        t = t + dt2;
    }
    zapis_do_pliku_wynik("Tekstowo/roz_analityczne_Lambda_04.txt", analit2, dt2, ile_t2);
}

void Method::rozw_laasonen(int a, fstream &file11) {
    double *b = new double[ile_x];
    double *wyn = new double[ile_x];
    double **diag = new double *[ile_x];
    for (int i = 0; i < ile_x; i++) diag[i] = new double[ile_x];
    //wyzerowanie macierzy na pocz¹tek
    for (int i = 0; i < ile_x; i++)
        for (int j = 0; j < ile_x; j++)
            diag[i][j] = 0;
    for (int j = 0; j < ile_x; j++)
        laasonen[0][j] = warunek_poczatkowy(D, tau, j); // Warunek początkowy
    for (int i = 0; i < ile_t; i++)
        laasonen[i][0] = 0;              //warunek brzegowy U(M,t) = 0
    for (int i = 0; i < ile_t; i++)
        laasonen[i][ile_x - 1] = 0;               //warunek brzegowy U(0,t) = 0
    /*Wypelnianie macierzy */
    for (int k = 1; k < ile_t; k++) {
        diag[0][0] = 1;
        b[0] = 1; //wynika z lewego warunku brzegowego

        for (int i = 1; i < ile_x - 1; i++) {
            diag[i][i] = -(1 + (2 * lambda));
            diag[i][i + 1] = lambda;
            diag[i][i - 1] = lambda;
            b[i] = -laasonen[k - 1][i];
        }
        b[ile_x - 1] = 0;  //wynika z prawego warunku brzegowego
        diag[ile_x - 1][ile_x - 1] = 1;
        if (a == 1) {
            thomas(diag, b, wyn);  //funkcja dla algorytmu Thomasa
        } else if (a == 2) {
            dekompozycja_LU(diag, b, wyn); //funkcja dla Dekompozycji LU
        }
        for (int i = 1; i < ile_x - 1; i++) {
            laasonen[k][i] = wyn[i];  // wyliczenie wektora rozwiazan
            file11 << laasonen[k][i] << ";";
        }
        file11 << "\n";
    }
    if (a == 1) {
        zapis_do_pliku_wynik("Tekstowo/rozw_metoda_laasonen_thomas.txt", laasonen, dt, ile_t);
    } else if (a == 2) {
        zapis_do_pliku_wynik("Tekstowo/rozw_metoda_laasonen_LU.txt", laasonen, dt, ile_t);
    }

}

//funkcja rozwiązujaca rozwnanie algorytmem Thomasa
void Method::thomas(double **A, double *b, double *wyn) {
    double *beta, *gamma;
    beta = new double[ile_x];
    gamma = new double[ile_x];
//obliczanie gammy i bety
    beta[0] = -(A[0][1] / A[0][0]);
    gamma[0] = (b[0] / A[0][0]);
    for (int i = 1; i < ile_x; i++) {
        if (i <= ile_x - 2)
            beta[i] = -((A[i][2 + i - 1]) / ((A[i][i - 1] * beta[i - 1]) + A[i][1 + i - 1]));

        gamma[i] = (b[i] - (A[i][i - 1] * gamma[i - 1])) / (A[i][i - 1] * beta[i - 1] + A[i][1 + i - 1]);
    }
    beta[ile_x - 1] = 0;
//obliczanie rozwiazania
    wyn[ile_x - 1] = gamma[ile_x - 1];
    for (int i = ile_x - 2; i >= 0; i--)
        wyn[i] = beta[i] * wyn[i + 1] + gamma[i];
//zwolnienie zarezerwowanej pamieci
    delete beta, gamma;
}

//funkcja obliczajaca blad dla metod pośrednich
void Method::f_blad(int a, fstream &file3) {
    double x = 0, t = 0;
    for (int j = 0; j < ile_t; j++) {
        for (int i = 0; i < ile_x; i++) {
            blad[j][i] = fabs(analit[j][i] - laasonen[j][i]);   //blad bezwzgledny
            if (blad[j][i] > blad_max[j]) blad_max[j] = blad[j][i]; //blad maks
            x = x + h;
            file3 <<t <<"\t" << x << "\t" << blad[j][i] << endl;
        }
        file3 << "\n";
        t = t + dt;
        x = 0;
    }
    if (a == 1) {
        zapis_do_pliku_wynik("Tekstowo/blad_bezwzgledny_dla_laasonen_thomas.txt", blad, dt, ile_t);
        zapis_do_pliku_blad("Tekstowo/blad_max_dla_laasonen_thomas.txt", blad_max, dt, ile_t);
    } else {
        zapis_do_pliku_wynik("Tekstowo/blad_bezwzgledny_dla_laasonen_LU.txt", blad, dt, ile_t);
        zapis_do_pliku_blad("Tekstowo/blad_max_dla_laasonen_LU.txt", blad_max, dt, ile_t);
    }
}

//funkcja obliczajaca blad dla metody poezposredniej
void Method::f_blad2(fstream &file4) {
    double x = 0, t = 0;
    for (int j = 0; j < ile_t2; j++) {
        for (int i = 0; i < ile_x; i++) {
            blad2[j][i] = fabs(analit2[j][i] - bezpo[j][i]); //blad bezwzgledny
            if (blad2[j][i] > blad_max2[j]) blad_max2[j] = blad2[j][i]; //blad maks
            x = x + h;
            file4 <<t <<"\t" << x << "\t" << blad2[j][i] << endl;
        }
        file4 << "\n";
        t = t + dt2;
        x = 0;
    }
    zapis_do_pliku_wynik("Tekstowo/blad_bezwzgledny_dla_KMB.txt", blad2, dt2, ile_t2);
    zapis_do_pliku_blad("Tekstowo/blad_max_dla_KMB.txt", blad_max2, dt2, ile_t2);
}

//funckja zapisujaca wyniki do pliku
void Method::zapis_do_pliku_wynik(char *nazwa, double **macierz, double dt, int ile_t) {
    ofstream file(nazwa, ios::out);
    double x = 0, t = 0;
    for (int j = 0; j < ile_t; j++) {
        for (int i = 0; i < ile_x; i++) {
            file <<t <<"\t" << x << "\t"  << macierz[j][i] << endl;
            x = x + h;
        }
        t = t + dt;
        x = 0;
    }
    file.close();
}

//funckja zapisujaca bledy do pliku
void Method::zapis_do_pliku_blad(char *nazwa, double *wektor, double dt, int ile_t) {
    ofstream file(nazwa, ios::out);
    double t = 0;
    file << nazwa << ", dt =  " << dt << endl << endl;
    for (int j = 0; j < ile_t; j++) {
        file <<t << "\t" << wektor[j] << endl;
        t = t + dt;
    }
    file.close();
}


int main() {

    Method method;
    fstream thomas_error, errors, results, anal_l04, anal_l1, thomas_result, lu_error, lu_result;
    anal_l04.open("Arkusze/Rozwiazanie_analityczne_lambda04.csv", fstream::out);
    anal_l1.open("Arkusze/Rozwiazanie_analityczne_lambda1.csv", ios::out);
    method.rozw_analityczne_lambda1(anal_l1);
    method.rozw_analityczne_lambda04(anal_l04);
    anal_l04.close();
    anal_l1.close();


    thomas_error.open("Arkusze/Blad_Thomas.csv", ios::out);
    thomas_result.open("Arkusze/Rozwiazanie_numeryczne_Thomas.csv", ios::out);
    method.rozw_laasonen(1, thomas_result);
    method.f_blad(1, thomas_error);
    thomas_error.close();
    thomas_result.close();

    lu_error.open("Arkusze/Blad_LU.csv", ios::out);
    lu_result.open("Arkusze/Rozwiazanie_numeryczne_LU.csv", ios::out);
    method.rozw_laasonen(2, lu_result);
    method.f_blad(2, lu_error);
    lu_error.close();
    lu_result.close();

    errors.open("Arkusze/Blad_KMB.csv", ios::out);
    results.open("Arkusze/Rozwizazanie_numeryczne_KMB.csv", ios::out);

    method.klas_met_bezp(results);
    method.f_blad2(errors);
    errors.close();
    results.close();

    system("gnuplot plotter.plt");
}




